# Demo application #
react, react-native, redux, redux-saga, internationalization, API requests, material UI, ...

# Installation: #
1. git clone https://bitbucker.org/ventym/wallet.git
2. cd wallet
3. npm install (or use yarn)
4. react-native run-android (or react-native run-ios)
