import { call, put, take, select } from 'redux-saga/effects'
import { sha256 } from 'react-native-sha256'

import actions from '../actions'
import * as api from '../api'
import AppError, { prepareError } from '../api/AppError'

const { PASSWORD_SALT } = require('../settings.json')
const { FIRST_NODE } = require('../settings.json')

export function* auth(action) {
  try {
    const { login, password } = action.payload
    const passwordHash = yield call(sha256, password + PASSWORD_SALT)
    const deviceInfo = yield select((state) => state.app.deviceInfo)

    // TODO use maximum rated node else FIRST_NODE
    const response = yield call(api.auth, { FIRST_NODE, login, passwordHash, deviceInfo })

    yield put(actions.session.auth.successful(response))
    yield put(actions.accounts.fetching())

    yield take(actions.session.deauth)
    // TODO use maximum rated node else FIRST_NODE
    yield call(api.deauth, {  FIRST_NODE })

  } catch(error) {
    yield put(actions.session.auth.rejected(prepareError(error)))
  }
}

export function* newWallet() {
  try {
    const deviceInfo = yield select((state) => state.app.deviceInfo)

    // TODO use maximum rated node else FIRST_NODE
    const response = yield call(api.newWallet, { FIRST_NODE, deviceInfo })

    yield put(actions.session.newWallet.successful(response))
    yield put(actions.accounts.fetching())

    yield take(actions.session.deauth)
    // TODO use maximum rated node else FIRST_NODE
    yield call(api.deauth, {  FIRST_NODE })

  } catch(error) {
    yield put(actions.session.newWallet.rejected(prepareError(error)))
  }
}
