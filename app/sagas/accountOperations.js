import { call, put, take, select } from 'redux-saga/effects'
import R from 'ramda'

import actions from '../actions'
import * as api from '../api'
import AppError, { prepareError } from '../api/AppError'

export function* transferMoney(action) {
  try {
    const sessionToken = yield select((state) => state.session.token)
    const { amount, toAccount } = action.payload

    const response = yield call(api.transferMoney, { sessionToken, amount, toAccount })

    if (response.done) {
      yield put(actions.accounts.transferMoney.successful())
    } else {
      yield put(actions.accounts.transferMoney.rejected(AppError.OPERATION_REJECTED))
    }

  } catch(error) {
    yield put(actions.accounts.transferMoney.rejected(prepareError(error)))
  }
}
