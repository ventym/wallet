import { call, put, take, all } from 'redux-saga/effects'
import {
  AsyncStorage,
  NetInfo,
} from 'react-native'
import DeviceInfo from 'react-native-device-info'

import actions from '../actions'
import * as api from '../api'
import AppError, { prepareError } from '../api/AppError'

const { FIRST_NODE } = require('../settings.json')

export default function* startApp() {
  try {
    let deviceInfo = yield all({
      uniqueId: call(DeviceInfo.getUniqueID),
      systemName: call(DeviceInfo.getSystemName),
      systemVersion: call(DeviceInfo.getSystemVersion),
      fontScale: call(DeviceInfo.getFontScale),
      deviceLocale: call(DeviceInfo.getDeviceLocale),
      networkConnected: call(NetInfo.isConnected.fetch)
    })

    if (!deviceInfo.networkConnected) {
      // TODO on iOS emulator NetInfo.isConnected.fetch always returns false (bug?)
      // throw new AppError(AppError.NETWORK_DISABLED)
    }

    // TODO read node list from local storage else FIRST_NODE
    const nodes = yield call(api.getNodes, { FIRST_NODE, deviceInfo })
    yield put(actions.nodes.fulfilled(nodes))

    let goToScreen = '/Auth'
    yield put(actions.app.loaded({ goToScreen, deviceInfo }))

  } catch(error) {
    yield put(actions.app.break(prepareError(error)))
  }
}
