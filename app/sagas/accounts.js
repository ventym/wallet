import { call, put, take, select } from 'redux-saga/effects'
import R from 'ramda'

import actions from '../actions'
import * as api from '../api'
import AppError, { prepareError } from '../api/AppError'

const { FIRST_NODE } = require('../settings.json')

export default function* fetchAccounts(action) {
  try {
    const sessionToken = yield select((state) => state.session.token)

    // TODO use maximum rated node else FIRST_NODE
    const response = yield call(api.fetchAccounts, { FIRST_NODE, sessionToken })

    const ids = R.pluck('_id', response) // [ {_id,vars}, {_id,vars} ] ==> [ _id, _id ]
    const list = R.indexBy(R.prop('_id'), response) // [ {_id,vars}, {_id,vars} ] ==> { _id:{_id,vars}, _id:{_id,vars} }

    yield put(actions.accounts.fulfilled({ list, ids }))

  } catch(error) {
    yield put(actions.accounts.rejected(prepareError(error)))
  }
}
