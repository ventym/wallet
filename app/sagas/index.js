import { all, takeLatest, fork, takeEvery } from 'redux-saga/effects'

import actions from '../actions'

import { auth, newWallet } from './auth'
import startApp from './startApp'
import fetchAccounts from './accounts'
import { transferMoney } from './accountOperations'

export default function* () {
  yield all([
    takeLatest(actions.session.auth.initiate, auth),
    takeLatest(actions.session.newWallet.initiate, newWallet),
    takeLatest(actions.accounts.fetching, fetchAccounts),
    takeEvery(actions.accounts.transferMoney.initiate, transferMoney),
    fork(startApp), // Start application
  ])
}
