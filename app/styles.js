import { StyleSheet } from 'react-native'
import { COLOR } from 'react-native-material-ui'

export const BACKGROUND_COLOR = '#eee'
export const NAVBAR_BACKGROUND_COLOR = COLOR.orange500
export const ACTIVITY_INDICATOR_COLOR = COLOR.orange500
export const TEXT_COLOR = 'black'
export const WELCOME_COLOR = COLOR.orange500
export const ERROR_CONTAINER_COLOR = '#fdd'
export const ERROR_TEXT_COLOR = 'black'
// export const ACCOUNT_ITEM_BACKGROUND_COLOR = '#eee'
export const ACTIVITY_BUTTON_COLOR = '#2196f3'

export default StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: BACKGROUND_COLOR,
  },
  containerPad: {
    flex: 1,
    backgroundColor: BACKGROUND_COLOR,
    padding: 20,
  },
  containerPad10: {
    flex: 1,
    backgroundColor: BACKGROUND_COLOR,
    padding: 10,
  },
  containerCenter: {
    flex: 1,
    backgroundColor: BACKGROUND_COLOR,
    padding: 20,
    justifyContent: 'center',
    alignItems: 'center',
  },
  input: {
    fontSize: 20,
  },
  txt: {
    fontSize: 20,
    color: TEXT_COLOR,
  },
  txtWelcome: {
    fontSize: 32,
    color: WELCOME_COLOR,
    // fontWeight: 'bold',
  },
  errorHeaderContainer: {
    backgroundColor: ERROR_CONTAINER_COLOR,
    padding: 10,
  },
  errorHeaderText: {
    fontSize: 14,
    color: ERROR_TEXT_COLOR,
  },
  accountItemContainer: {
    // backgroundColor: ACCOUNT_ITEM_BACKGROUND_COLOR,
    padding: 10,
  },
  txtSmall: {
    fontSize: 12,
  },
  button: {
    marginTop: 10,
  },
  activityButton: {
    backgroundColor: ACTIVITY_BUTTON_COLOR,
  },
  activityButtonIcon: {
  },
})

export const uiTheme = {
  palette: {
    primaryColor: NAVBAR_BACKGROUND_COLOR,
  },
  toolbar: {
    container: {
      height: 50,
    },
  },
}
