import { handleActions } from 'redux-actions'
import actions from '../actions'

const initialState = {
  errorCode: '',
  transferInProcess: false,
}

export default handleActions({
  [actions.accounts.transferMoney.initiate]: (state, action) => {
    return { ...state, errorCode: '', transferInProcess: true }
  },

  [actions.accounts.transferMoney.successful]: (state, action) => {
    return { ...state, errorCode: '', transferInProcess: false }
  },

  [actions.accounts.transferMoney.rejected]: (state, action) => {
    return { ...state, errorCode: action.payload, transferInProcess: false }
  },
}, initialState)
