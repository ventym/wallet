import { handleActions } from 'redux-actions'
import actions from '../actions'

const initialState = {
  list: {},
  ids: [],

  errorCode: '',
  isLoading: false,
  isLoaded: false,
}

export default handleActions({
  // [actions.nodes.fetching]: (state, action) => {
  //   return { ...state, errorCode: '', isLoading: true }
  // },

  [actions.nodes.fulfilled]: (state, action) => {
    return { ...state, errorCode: '', isLoading: false, isLoaded: true, list: action.payload.list, ids: action.payload.ids }
  },

  // [actions.nodes.rejected]: (state, action) => {
  //   return { ...state, errorCode: action.payload, isLoading: false }
  // },
}, initialState)
