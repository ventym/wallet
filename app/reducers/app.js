import { handleActions } from 'redux-actions'
import actions from '../actions'

const initialState = {
  goToScreen: '',
  deviceInfo: {},

  errorCode: '',
  isLoaded: false,
}

export default handleActions({
  [actions.app.loaded]: (state, action) => {
    return { ...state, errorCode: '', isLoaded: true, goToScreen: action.payload.goToScreen, deviceInfo: action.payload.deviceInfo }
  },

  [actions.app.break]: (state, action) => {
    return { ...state, errorCode: action.payload, isLoaded: false }
  },
}, initialState)
