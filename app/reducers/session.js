import { handleActions } from 'redux-actions'
import actions from '../actions'

const initialState = {
  token: '',

  errorCode: '',
  isProcessing: false,
}

export default handleActions({
  [actions.session.auth.initiate]: (state, action) => {
    return { ...state, errorCode: '', isProcessing: true }
  },

  [actions.session.auth.successful]: (state, action) => {
    return { ...state, errorCode: '', isProcessing: false, token: action.payload.token }
  },

  [actions.session.auth.rejected]: (state, action) => {
    return { ...state, errorCode: action.payload, isProcessing: false, token: '' }
  },

  [actions.session.deauth]: (state, action) => {
    return { ...state, errorCode: '', isProcessing: false, token: '' }
  },

  [actions.session.newWallet.initiate]: (state, action) => {
    return { ...state, errorCode: '', isProcessing: true }
  },

  [actions.session.newWallet.successful]: (state, action) => {
    return { ...state, errorCode: '', isProcessing: false, token: action.payload.token }
  },

  [actions.session.newWallet.rejected]: (state, action) => {
    return { ...state, errorCode: action.payload, isProcessing: false, token: '' }
  },

}, initialState)
