import { handleActions } from 'redux-actions'
import actions from '../actions'

const initialState = {
  list: {},
  ids: [],

  errorCode: '',
  isLoading: false,
  isLoaded: false,
}

export default handleActions({
  [actions.accounts.fetching]: (state, action) => {
    return { ...state, errorCode: '', isLoading: true }
  },

  [actions.accounts.fulfilled]: (state, action) => {
    return { ...state, errorCode: '', isLoading: false, isLoaded: true, list: action.payload.list, ids: action.payload.ids }
  },

  [actions.accounts.rejected]: (state, action) => {
    return { ...state, errorCode: action.payload, isLoading: false }
  },
}, initialState)
