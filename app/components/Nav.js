import React from 'react'
import {
  Text,
  View,
  Platform,
  UIManager,
  // Animated,
  // Easing,
} from 'react-native'
import { withRouter } from 'react-router-native'
import { Toolbar, IconToggle } from 'react-native-material-ui'

import styles, { NAVBAR_COLOR } from '../styles'

class Nav extends React.Component {
  constructor() {
    super()
    if (Platform.OS === 'android') {
      UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true)
    }
  }

  render() {
    return (
      <Toolbar
        leftElement={this.props.history.index ? 'arrow-back' : 'menu'}
        centerElement={this.props.title || ''}
        onLeftElementPress={this.props.history.index ? this.onBackClicked : this.onMenuClicked}
        rightElement={this.props.match.path=='/AccountDetails/:_id' ? 'more-vert' : null}
        onRightElementPress={this.onRightButtonClicked}
      />
    )
  }

  onBackClicked = () => {
    this.props.history.goBack()
  }

  onMenuClicked = () => {
    this.props.onMenuClicked && this.props.onMenuClicked()
  }

  onRightButtonClicked = () => {
    this.props.onRightButtonClicked && this.props.onRightButtonClicked()
  }
}

export default withRouter(Nav)
