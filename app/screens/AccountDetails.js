import React from 'react'
import {
  View,
  Text,
  Button,
  TextInput,
  ScrollView,
} from 'react-native'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { getStatusBarHeight } from 'react-native-status-bar-height'

import actions from '../actions'
import styles from '../styles'
import i18n from '../i18n'
import Nav from '../components/Nav'

class AccountDetailsScreen extends React.Component {
  componentDidMount() {
    // TODO scroll to previous position
  }

  render() {
    const { account } = this.props
    return (
      <View style={[styles.container, { marginTop: getStatusBarHeight(true) }]}>
        <Nav title={i18n.t('accountDetails.title')} />
        <ScrollView style={styles.containerPad10}>
          <Text style={styles.txt}>{account.currency + ' ' + account.amount}</Text>
          <Text style={styles.txtSmall}>{account.issuer_name || ''}</Text>
          <Text style={styles.txtSmall}>{account.product_name || ''}</Text>
          <View style={styles.button}>
            <Button title={i18n.t('accountDetails.showOperations')} onPress={this.showOperations} />
          </View>
          <TextInput
            placeholder={i18n.t('accountDetails.amountPlaceholder')}
            ref={ref => this._amountInput = ref}
            keyboardType='numeric'
            autoCapitalize='none'
            returnKeyType='next'
            spellCheck={false}
            onSubmitEditing={() => this._toAccountInput.focus()}
            editable={!this.props.accountOperations.transferInProcess}
            style={styles.input}
          />
          <TextInput
            placeholder={i18n.t('accountDetails.toAccountPlaceholder')}
            ref={ref => this._toAccountInput = ref}
            keyboardType='default'
            autoCapitalize='none'
            returnKeyType='done'
            spellCheck={false}
            onSubmitEditing={() => this.transferMoney()}
            editable={!this.props.accountOperations.transferInProcess}
            style={styles.input}
          />
          <View style={styles.button}>
            <Button title={i18n.t('accountDetails.transferMoney')} onPress={this.transferMoney} disabled={this.props.accountOperations.transferInProcess} />
          </View>
        </ScrollView>
      </View>
    )
  }

  showOperations = () => {
    // TODO
  }

  transferMoney = () => {
    this.props.transferMoney({
      amount: this._amountInput.value,
      toAccount: this._toAccountInput.value,
    })
  }
}

const mapStateToProps = (state, ownProps) => ({
  account: state.accounts.list[ownProps.match.params['_id']],
  accountOperations: state.accountOperations,
})

const mapDispatchToProps = dispatch => bindActionCreators({
  transferMoney: actions.accounts.transferMoney.initiate,
}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(AccountDetailsScreen)
