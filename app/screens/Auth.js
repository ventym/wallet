import React from 'react'
import {
  View,
  Text,
  Button,
  TextInput,
  ActivityIndicator,
} from 'react-native'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Redirect } from 'react-router-native'
import { getStatusBarHeight } from 'react-native-status-bar-height'

import actions from '../actions'
import styles from '../styles'
import i18n from '../i18n'

class Auth extends React.Component {
  componentDidMount() {
    this._loginInput && this._loginInput.focus()
  }

  render() {
    if (this.props.session.token) {
      return <Redirect to='/Wallet' />
    }

    return (
      <View style={[styles.containerPad, { marginTop: getStatusBarHeight(true) }]}>
        <TopBlock {...this.props} />
        <TextInput
          placeholder={i18n.t('auth.loginPlaceholder')}
          ref={ref => this._loginInput = ref}
          keyboardType='email-address'
          autoCapitalize='none'
          returnKeyType='next'
          spellCheck={false}
          onSubmitEditing={() => this._passwordInput.focus()}
          editable={!this.props.session.isProcessing}
          style={styles.input}
        />
        <TextInput
          placeholder={i18n.t('auth.passwordPlaceholder')}
          ref={ref => this._passwordInput = ref}
          secureTextEntry={true}
          autoCapitalize='none'
          returnKeyType='done'
          spellCheck={false}
          onSubmitEditing={this.startAuth}
          editable={!this.props.session.isProcessing}
          style={styles.input}
        />
        <View style={[styles.button, { marginTop: 40 }]}>
          <Button
            title={i18n.t('auth.authButton')}
            onPress={this.startAuth}
            disabled={this.props.session.isProcessing}
          />
        </View>
        <Text style={[styles.txt, { textAlign: 'center', marginTop: 10 }]}>
          {i18n.t('auth.or')}
        </Text>
        <View style={styles.button}>
          <Button
            title={i18n.t('auth.newButton')}
            onPress={this.newWallet}
            disabled={this.props.session.isProcessing}
          />
        </View>
      </View>
    )
  }

  startAuth = () => {
    this.props.startAuth({
      login: this._loginInput.value,
      password: this._passwordInput.value,
    })
  }

  newWallet = () => {
    this.props.newWallet()
  }
}

const mapStateToProps = state => ({
  session: state.session,
})

const mapDispatchToProps = dispatch => bindActionCreators({
  startAuth: actions.session.auth.initiate,
  newWallet: actions.session.newWallet.initiate,
}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(Auth)

const TopBlock = (props) => {
  if (props.session.isProcessing) {
    return (
      <View style={{ marginVertical: 10 }}>
        <ActivityIndicator />
      </View>
    )
  } else if (props.session.errorCode) {
    return (
      <View style={styles.errorHeaderContainer}>
        <Text style={styles.errorHeaderText}>{i18n.t('error.' + props.session.errorCode)}</Text>
      </View>
    )
  } else
    return null
}
