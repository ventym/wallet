import React from 'react'
import {
  View,
  Text,
  Button,
  FlatList,
  RefreshControl,
  ActivityIndicator,
  Platform,
  UIManager,
  Animated,
  Easing,
} from 'react-native'
import Touchable from 'react-native-platform-touchable'
import { ActionButton } from 'react-native-material-ui'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { getStatusBarHeight } from 'react-native-status-bar-height'

import actions from '../actions'
import styles, { ACTIVITY_INDICATOR_COLOR } from '../styles'
import i18n from '../i18n'
import Nav from '../components/Nav'

const AnimatedActionButton = Animated.createAnimatedComponent(ActionButton)

class WalletScreen extends React.Component {
  constructor() {
    super()
    this.state = {
      activityButtonScale: new Animated.Value(1),
    }
    if (Platform.OS === 'android') {
      UIManager.setLayoutAnimationEnabledExperimental && UIManager.setLayoutAnimationEnabledExperimental(true)
    }
  }

  componentDidMount() {
    // TODO scroll screen to previous position
  }

  renderAccount = ({ item }) => <AccountItem account={this.props.accounts.list[item]} onPress={this.goToAccountDetails} />

  render() {
    return (
      <View style={[styles.container, { marginTop: getStatusBarHeight(true) }]}>
        <Nav title={i18n.t('wallet.title')} onMenuClicked={this.showDrawer} />
        {this.renderContent()}
      </View>
    )
  }

  renderContent = () => {
    const { accounts } = this.props

    if (!accounts.isLoaded && accounts.isLoading) {
      return <WalletLoading />
    }

    if (!accounts.isLoaded && accounts.errorCode) {
      return <WalletLoadingError text={i18n.t('error' + accounts.errorCode)} reloadAccounts={this.props.reloadAccounts} />
    }

    if (accounts.isLoaded && accounts.ids.length === 0) {
      return <EmptyWallet goToNewAccount={this.goToNewAccount} />
    }

    return (
      <View style={styles.container}>
        {accounts.errorCode ? <ErrorHeader text={i18n.t('error' + accounts.errorCode)} /> : null}
        <FlatList
          data={accounts.ids}
          renderItem={this.renderAccount}
          keyExtractor={item => item}
          refreshControl={
            <RefreshControl
              refreshing={this.props.accounts.isLoading}
              onRefresh={this.props.reloadAccounts}
            />
          }
        />
        <AnimatedActionButton
          onPress={this.goToNewAccount}
          style={{
            transform: [{ scale: this.state.activityButtonScale }],
            container: styles.activityButton,
            icon: styles.activityButtonIcon,
          }}
        />
      </View>
    )
  }

  showDrawer = () => {
    // TODO
  }

  goToAccountDetails = (accountId) => {
    this.props.history.push('/AccountDetails/' + accountId)
  }

  goToNewAccount = () => {
    this.animateActivityButton()
    this.props.history.push('/NewAccount')
  }

  animateActivityButton = () => {
    // TODO
  }
}

const mapStateToProps = state => ({
  accounts: state.accounts,
})

const mapDispatchToProps = dispatch => bindActionCreators({
  reloadAccounts: actions.accounts.fetching,
}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(WalletScreen)

const ErrorHeader = (props) => (
  <View style={styles.errorHeaderContainer}>
    <Text style={styles.errorHeaderText}>{props.text}</Text>
  </View>
)

const WalletLoading = (props) => (
  <View style={styles.containerCenter}>
    <ActivityIndicator size="large" color={ACTIVITY_INDICATOR_COLOR} />
  </View>
)

const EmptyWallet = (props) => (
  <View style={styles.containerPad10}>
    <Text style={styles.txt}>
      {i18n.t('wallet.emptyWallet')}
    </Text>
    <View style={styles.button}>
      <Button title={i18n.t('wallet.goToNewAccount')} onPress={props.goToNewAccount} />
    </View>
  </View>
)

const WalletLoadingError = (props) => (
  <View style={styles.containerCenter}>
    <Text style={styles.txt}>{props.text}</Text>
    <Button title={i18n.t('wallet.reloadWallet')} onPress={props.reloadAccounts}/>
  </View>
)

const AccountItem = (props) => {
  const { account } = props
  return (
    <Touchable
      onPress={() => props.onPress(account._id)}
      style={styles.accountItemContainer}
    >
      <View>
        <Text style={styles.txt}>
          {account.currency + ' ' + account.amount}
        </Text>
        <Text style={styles.txtSmall}>
          {(account.issuer_name || '') + ' ' + (account.product_name || '')}
        </Text>
      </View>
    </Touchable>
  )
}
