import React from 'react'
import {
  View,
  Text,
  Button,
  ScrollView,
} from 'react-native'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { getStatusBarHeight } from 'react-native-status-bar-height'

import actions from '../actions'
import styles from '../styles'
import i18n from '../i18n'
import Nav from '../components/Nav'

class NewAccountScreen extends React.Component {
  componentDidMount() {
    // TODO scroll to previous position
  }

  render() {
    return (
      <View style={[styles.container, { marginTop: getStatusBarHeight(true) }]}>
        <Nav title={i18n.t('newAccount.title')} />
        <ScrollView style={styles.containerPad10}>
          <Text style={styles.txt}>New account</Text>
          <View style={styles.button}>
            <Button title={i18n.t('newAccount.addAccount')} onPress={this.addAccount} />
          </View>
        </ScrollView>
      </View>
    )
  }

  addAccount = () => {
    // TODO
  }
}

const mapStateToProps = state => ({
  accounts: state.accounts,
})

const mapDispatchToProps = dispatch => bindActionCreators({
  // addAccount: actions.accounts.add.initiate,
}, dispatch)

export default connect(mapStateToProps, mapDispatchToProps)(NewAccountScreen)
