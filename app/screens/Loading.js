import React from 'react'
import {
  View,
  Text,
  Button,
  Animated,
  Easing,
  Platform,
} from 'react-native'
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'
import { Redirect } from 'react-router-native'
import Icon from 'react-native-vector-icons/MaterialIcons'

import AppError from '../api/AppError'
import actions from '../actions'
import styles, { WELCOME_COLOR } from '../styles'
import i18n from '../i18n'

const AnimatedIcon = Animated.createAnimatedComponent(Icon)

class LoadingScreen extends React.Component {
  constructor() {
    super()
    this.state = {
      animationFinished: false,
      scale: new Animated.Value(1),
      logoIcon: 'monetization-on',
    }
  }

  componentDidMount() {
    this.rotateLogo()
  }

  render() {
    if (this.props.app.errorCode && this.state.animationFinished) {
      return (
        <LoadingError
          errorCode={this.props.app.errorCode}
          openPhoneSettings={this.openPhoneSettings}
        />
      )
    }

    if (this.props.app.isLoaded && this.state.animationFinished) {
      return <Redirect to={this.props.app.goToScreen} />
    }

    return (
      <View style={styles.containerCenter}>
        <AnimatedIcon name={this.state.logoIcon} size={80} color={WELCOME_COLOR} style={{ transform: [{ scaleX: this.state.scale }] }} />
        <Text style={[styles.txtWelcome, {marginTop: 20}]}>
          {i18n.t('loading.welcome')}
        </Text>
      </View>
    )
  }

  openPhoneSettings = () => {
    // TODO
  }

  rotateLogo = () => {
    Animated.sequence([
      Animated.delay(300),
      Animated.timing(this.state.scale, {
        toValue: 0,
        duration: 300,
        easing: Easing.in, //Easing.linear,
        useNativeDriver: Platform.OS === 'android',
      }),
      Animated.timing(this.state.scale, {
        toValue: 1,
        duration: 300,
        easing: Easing.in, //Easing.linear,
        useNativeDriver: Platform.OS === 'android',
      }),
      Animated.delay(500),
    ]).start(() => {
      this.setState({ animationFinished: true })
    })
  }
}

const mapStateToProps = state => ({
  app: state.app,
})

export default connect(mapStateToProps)(LoadingScreen)

const LoadingError = (props) => (
  <View style={styles.containerPad10}>
    <Text style={styles.txt}>
      {i18n.t('error.' + props.errorCode)}
    </Text>
    {
      props.errorCode === AppError.NETWORK_DISABLED ? (
        <View style={styles.button}>
          <Button title={i18n.t('loading.settingsButton')} onPress={props.openPhoneSettings} />
        </View>
      ) : null
    }
  </View>
)
