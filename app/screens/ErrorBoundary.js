import React from 'react'
import {
  View,
  Text,
} from 'react-native'

import styles from '../styles'
import i18n from '../i18n'

export default class ErrorBoundary extends React.Component {
  constructor(props) {
    super(props)
    this.state = { hasError: false }
  }

  componentDidCatch(error, info) {
    this.setState({ hasError: true })
    // logErrorToMyService(error, info)
  }

  render() {
    if (this.state.hasError) {
      return (
        <View style={styles.containerCenter}>
          <Text style={styles.txt}>{i18n.t('error.errorBoundary')}</Text>
        </View>
      )
    }
    return this.props.children
  }
}
