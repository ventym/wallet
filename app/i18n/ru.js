import AppError from '../api/AppError'

export default {
  error: {
    errorBoundary: 'Извините, я поломалась :-(',
    [AppError.NETWORK_DISABLED]: 'Сеть недоступна',
    [AppError.API_UNREACHANBLE]: 'Сервер недоступен',
    [AppError.WRONG_API_ANSWER]: 'Сервер гонит',
    [AppError.ACCESS_DENIED]: 'Доступ запрещён',
    [AppError.APP_ERROR]: 'Ошибка в приложении',
    [AppError.OPERATION_REJECTED]: 'Операция отклонена',
  },
  loading: {
    welcome: 'WALLET',
    error: 'Ошибка',
    settingsButton: 'Открыть настройки',
  },
  auth: {
    authButton: 'Войдите',
    or: 'или',
    newButton: 'Создайте свой кошелёк',
    loginPlaceholder: 'Логин',
    passwordPlaceholder: 'Пароль',
  },
  wallet: {
    title: 'Кошелёк',
    emptyWallet: 'В вашем кошельке пусто. Просто добавьте счета',
    reloadWallet: 'Повторить',
    goToNewAccount: 'Добавить счёт',
  },
  accountDetails: {
    title: 'Счёт',
    showOperations: 'Показать операции',
    transferMoney: 'Перевести деньги',
    amountPlaceholder: '0',
    toAccountPlaceholder: 'На счёт',
  },
  newAccount: {
    title: 'Новый счёт',
    addAccount: 'Добавить счёт',
  },
}
