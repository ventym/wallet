import i18n from 'react-native-i18n'
import en from './en'
import ru from './ru'

i18n.fallbacks = true

i18n.translations = {
  en,
  ru,
}

i18n.defaultLocale = 'en'

export default i18n
