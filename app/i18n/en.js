import AppError from '../api/AppError'

export default {
  error: {
    errorBoundary: 'Sorry, application crashed :-(',
    [AppError.NETWORK_DISABLED]: 'Network disabled on your phone',
    [AppError.API_UNREACHANBLE]: 'Server unreachable',
    [AppError.WRONG_API_ANSWER]: 'Wrong API answer',
    [AppError.ACCESS_DENIED]: 'Access denied',
    [AppError.APP_ERROR]: 'Application error',
    [AppError.OPERATION_REJECTED]: 'Operation rejected',
  },
  loading: {
    welcome: 'WALLET',
    error: 'Error',
    settingsButton: 'Open settings',
  },
  auth: {
    authButton: 'Sign in',
    or: 'or',
    newButton: 'Create new wallet',
    loginPlaceholder: 'Login',
    passwordPlaceholder: 'Password',
  },
  wallet: {
    title: 'Wallet',
    emptyWallet: 'No accounts in your wallet. Just accept an account',
    reloadWallet: 'Reload',
    goToNewAccount: 'Add account',
  },
  accountDetails: {
    title: 'Account',
    showOperations: 'Show operations',
    transferMoney: 'Transfer money',
    amountPlaceholder: '0',
    toAccountPlaceholder: 'To account',
  },
  newAccount: {
    title: 'New account',
    addAccount: 'Add account',
  },
}
