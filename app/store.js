/* eslint-disable global-require */
/* eslint-disable no-undef */
import { createStore, combineReducers, applyMiddleware } from 'redux'
import logger from 'redux-logger'
import createSagaMiddleware from 'redux-saga'

import app from './reducers/app'
import nodes from './reducers/nodes'
import session from './reducers/session'
import accounts from './reducers/accounts'
import accountOperations from './reducers/accountOperations'

import rootSaga from './sagas'

export default function configureStore(initialState) {
  const rootReducer = combineReducers({
    app,
    nodes,
    session,
    accounts,
    accountOperations,
  })

  const sagaMiddleware = createSagaMiddleware()
  let middleware = [
    sagaMiddleware,
  ]
  if (__DEV__) {
    const reduxImmutableStateInvariant = require('redux-immutable-state-invariant').default()
    middleware = [...middleware, reduxImmutableStateInvariant, logger]
  }

  const store = createStore(
    rootReducer,
    initialState,
    applyMiddleware(...middleware)
  )

  sagaMiddleware.run(rootSaga) // Start application

  return store
}
