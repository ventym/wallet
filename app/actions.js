import { createActions } from 'redux-actions'

export default createActions({
  app: {
    loaded: null,
    break: null,
  },
  nodes: {
    fulfilled: null,
  },
  session: {
    auth: {
      initiate: null,
      successful: null,
      rejected: null,
    },
    deauth: null,
    newWallet: {
      initiate: null,
      successful: null,
      rejected: null,
    },
  },
  accounts: {
    fetching: null,
    fulfilled: null,
    rejected: null,
    add: {
      initiate: null,
      successful: null,
      rejected: null,
    },
    transferMoney: {
      initiate: null,
      successful: null,
      rejected: null,
    },
  },
})
