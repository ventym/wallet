import React from 'react'
import { Provider } from 'react-redux'
import { NativeRouter, Route, AndroidBackButton } from 'react-router-native'
import { ThemeProvider } from 'react-native-material-ui'

import { uiTheme } from './styles'

import configureStore from './store'

import ErrorBoundary from './screens/ErrorBoundary'
import LoadingScreen from './screens/Loading'
import AuthScreen from './screens/Auth'
import WalletScreen from './screens/Wallet'
import AccountDetailsScreen from './screens/AccountDetails'
import NewAccountScreen from './screens/NewAccount'

const store = configureStore()

export default () => (
  <ErrorBoundary>
    <Provider store={store}>
      <NativeRouter>
        <ThemeProvider uiTheme={uiTheme}>
          <AndroidBackButton>
            <Route exact path='/' component={LoadingScreen} />
            <Route path='/Auth' component={AuthScreen} />
            <Route path='/Wallet' component={WalletScreen} />
            <Route path='/AccountDetails/:_id' component={AccountDetailsScreen} />
            <Route path='/NewAccount' component={NewAccountScreen} />
          </AndroidBackButton>
        </ThemeProvider>
      </NativeRouter>
    </Provider>
  </ErrorBoundary>
)
