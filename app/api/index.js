import AppError from './AppError'
import { delay, fetchAPI } from './utils'

export async function getNodes({ node, deviceInfo }) {
  // let response = await fetchAPI('GET', node + 'nodes', '', { deviceInfo })
  // response = await response.json()
  let response = require('./fakeAPI/getNodes.json')
  await delay(300)

  return response
}


export async function auth({ node, login, passwordHash, deviceInfo }) {
  // TODO
  // let response = await fetchAPI('POST', node + 'session', '', { login, passwordHash, deviceInfo })
  // response = await response.json()
  let response = require('./fakeAPI/auth.json')
  await delay(800)

  // TODO
  // if (!validateJSON(response)) throw new AppError(AppError.WRONG_API_ANSWER)

  return response
}


export async function newWallet({ node, deviceInfo }) {
  // TODO
  // let response = await fetchAPI('PUT', node + 'session', '', { deviceInfo })
  // response = await response.json()
  let response = require('./fakeAPI/newWallet.json')
  await delay(800)

  // TODO
  // if (!validateJSON(response)) throw new AppError(AppError.WRONG_API_ANSWER)

  return response
}


export async function deauth({ node, sessionToken }) {
  // TODO
  // let response = await fetchAPI('DELETE', node + 'session', sessionToken)
}


export async function fetchAccounts({ node, sessionToken }) {
  // TODO
  // let response = await fetchAPI('GET', node + 'accounts', sessionToken)
  // response = await response.json()
  let response = require('./fakeAPI/accounts-empty.json')
  if (sessionToken === '3ad4e358eafdd709aacb93891a92d5c9cf7bd6cfdfc6f26cb32ee95f525a0ed8') {
    response = require('./fakeAPI/accounts.json')
  }
  await delay(800)

  // TODO
  // if (!validateJSON(response)) throw new AppError(AppError.WRONG_API_ANSWER)

  return response
}

export async function transferMoney({ node, sessionToken, amount, toAccount }) {
  // TODO
  // let response = await fetchAPI('GET', node + 'accounts', { sessionToken, amount, toAccount })
  // response = await response.json()
  let response = { done: true }
  await delay(800)

  // TODO
  // if (!validateJSON(response)) throw new AppError(AppError.WRONG_API_ANSWER)

  return response
}
