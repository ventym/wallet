function AppError(errorCode){
  this.name = 'AppError'
  this.message = 'AppError ' + errorCode
  this.errorCode = errorCode
  // this.stack = new Error().stack
}
AppError.prototype = Object.create(Error.prototype)
// AppError.prototype.constructor = AppError

AppError.NETWORK_DISABLED = 'NETWORK_DISABLED'
AppError.API_UNREACHANBLE = 'API_UNREACHANBLE'
AppError.WRONG_API_ANSWER = 'WRONG_API_ANSWER'
AppError.ACCESS_DENIED = 'ACCESS_DENIED'
AppError.APP_ERROR = 'APP_ERROR'
AppError.OPERATION_REJECTED = 'OPERATION_REJECTED'

export default AppError

export function prepareError(error) {
  if (__DEV__) console.log(error, error.stack)

  if (error instanceof AppError) {
    return error.errorCode
  } else if (error instanceof SyntaxError) {
    return AppError.WRONG_API_ANSWER
  } else {
    return AppError.APP_ERROR
  }
}
