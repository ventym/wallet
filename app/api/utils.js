export const fetchAPI = (method, url, sessionToken = '', body = undefined) => {
  let headers = {
    'Accept': 'application/json',
    'Content-Type': 'application/json',
    'token': sessionToken,
  }
  return fetch(url, {
    method,
    headers,
    body: body ? JSON.stringify(body) : undefined,
  })
}

export const delay = (msec) => new Promise(resolve => setTimeout(resolve, msec))
